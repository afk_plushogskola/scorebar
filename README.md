# README #

### Introduction ###

ScanQuest, known before as ScoreBar.
"Scanning barcodes will probably never get more fun than this!"

"The Evolution is real, and the proof is right in front of you"
AFK-team new released and revamped Android application ScoreBar takes us to another level.

"You will never look at a barcode the same again" - Alex

* Latest Version: 2.1.2

### What is this Android application? ###

You are the adventurer and the world is your playground. Your purpose is to purge the evil bosses from hell!

Scan barcodes to gain EXP and level up in Adventure mode, test yourself in Arena.
Explore the world, complete achievements and find secret easter eggs to gain additional EXP.

A barcode can only be scanned once in Adventure Mode to gain EXP, but endless times in Arena as a weapon.


### How do I get set up? ###

* Go to "Downloads" tab in our repository and download apk.

* Set your phone to trust untrusted sources.

* Install downloaded apk on your phone.

* Download and install ZXing from Google Play Store (most phone models will be prompted to download when scanning if not already installed on your local device.)

* Run ScanQuest and start playing!

### Who do I talk to? ###

If you find any bugs please report it in our "Issues" tab or if you have any questions, please contact us at:

afkscorebar@gmail.com

###Known bugs###

* Text in spinner is out of place on some phone models.

###Troubleshooting###

* Make sure you have ZXing Barcode Scanner installed on your local device.

* Have a functional camera on your phone.

* For the best result when scanning, make sure your surrounding is lit and the camera have autofocus.

* Try delete and reinstall latest version. Please keep in mind that all your saves will be lost when deleted.

* If any of the above does not solve your problem, do not hesitate to contact us at afkscorebar@gmail.com.