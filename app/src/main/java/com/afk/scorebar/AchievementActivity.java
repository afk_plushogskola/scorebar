package com.afk.scorebar;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.afk.scorebar.helper.AchievementListAdapter;
import com.afk.scorebar.helper.DbAdapter;
import com.afk.scorebar.helper.EasterEggListAdapter;
import com.afk.scorebar.model.Achievement;
import com.afk.scorebar.model.EasterEgg;
import com.drivemode.android.typeface.TypefaceHelper;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.ArrayList;

/**
 * <p>Shows listview with either {@code Achievement} or {@code EasterEgg} instances</p>
 * <p>Supports left swiping for back navigation</p>
 * <p>Uses two  extendend {@code BaseAdapter}'s:
 * <ol>
 *     <l>{@code EasterEggListAdapter}</l>
 *     <l> {@code AchievementListAdapter}</l>
 * </ol>
 * </p>
 */
public class AchievementActivity extends ActionBarActivity {
    private SwipeBackLayout swipeBackLayout;
    private ListView achievlistView;
    private ListView easterEgglistView;
    private TextView achievlabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TypefaceHelper.initialize(getApplication());
        setContentView(TypefaceHelper.getInstance().setTypeface(this, R.layout.activity_achievement, "exocet.otf"));
        achievlabel = (TextView) findViewById(R.id.achievlabel);
        swipeBackLayout = (SwipeBackLayout) findViewById(R.id.swipeBackLayoutA);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);

        new PopulateAchievements().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_achievement, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onSwitchClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            new PopulateEasterEggs().execute();

        } else {
            new PopulateAchievements().execute();

        }

    }


    /**
     * <p>Retrieves {@code EasterEgg} ArrayList from {@code DbAdapter} and populates it to easterEggs}</p>
     */
    private class PopulateEasterEggs extends AsyncTask<Void, Void, ArrayList<EasterEgg>> {
        @Override
        protected ArrayList<EasterEgg> doInBackground(Void... params) {

            DbAdapter adapter = new DbAdapter(AchievementActivity.this);
            return adapter.retrieveEasterEggs();
        }

        @Override
        protected void onPostExecute(ArrayList<EasterEgg> easterEggs) {


            easterEgglistView = (ListView) findViewById(R.id.achievlist);
            EasterEggListAdapter easterAdapter =
                    new EasterEggListAdapter(AchievementActivity.this, easterEggs);
            easterEgglistView.setAdapter(easterAdapter);
            achievlabel.setText("Easter Eggs");

        }
    }

    /**
     * <p>Retrieves {@code Achievement} ArrayList from {@code DbAdapter} and populates it to achievements}</p>
     */
    private class PopulateAchievements extends AsyncTask<Void, Void, ArrayList<Achievement>> {
        @Override
        protected ArrayList<Achievement> doInBackground(Void... params) {

            DbAdapter adapter = new DbAdapter(AchievementActivity.this);
            return adapter.retrieveAchievements();
        }

        @Override
        protected void onPostExecute(ArrayList<Achievement> achievementList) {

            achievlistView = (ListView) findViewById(R.id.achievlist);
            AchievementListAdapter achievAdapter =
                    new AchievementListAdapter(AchievementActivity.this, achievementList);
            achievlistView.setAdapter(achievAdapter);
            achievlabel.setText("Achievements");

        }
    }


}
