package com.afk.scorebar;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afk.scorebar.helper.DatePickerDialogFragment;
import com.afk.scorebar.helper.DbAdapter;
import com.afk.scorebar.helper.SpinnerOnItemSelect;
import com.afk.scorebar.helper.StatsListAdapter;
import com.afk.scorebar.helper.Toaster;
import com.afk.scorebar.model.Achievement;
import com.afk.scorebar.model.EasterEgg;
import com.afk.scorebar.model.Scan;
import com.drivemode.android.typeface.TypefaceHelper;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Retrieves and displays a list of scans with date and EXP gained from DB. <br>
 * The sorting can be altered with a {@code spinner}. <p>
 * Icons for an achievement or an easter egg will display if it was
 * found on that specific scan in the {@code ListView},
 * and will show a {@code toast} with name and description when clicked.<br>
 */
public class StatisticsActivity extends ActionBarActivity implements DatePickerDialog.OnDateSetListener {

    private SwipeBackLayout swipeBackLayout;
    private ListView listView;

    private ArrayList<Scan> scanList;

    private TextView fromDateTV,
            toDateTV,
            startDate,
            endDate;

    private Spinner spinnerSortBy;

    private boolean sortByDate;

    private static final String FRAGMENT_FROM = "from",
            FRAGMENT_TO = "to";

    private String START_DATE_INTERVAL,
            END_DATE_INTERVAL;

    private String position;

    private Calendar calendar;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Achievement achievement;
    private EasterEgg easterEgg;

    private String achievDescription,
            easterDescription,
            achievName,
            easterName;

    private Toaster toaster = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TypefaceHelper.initialize(getApplication());
        setContentView(TypefaceHelper.getInstance().setTypeface(this, R.layout.activity_statistics, "exocet.otf"));

        swipeBackLayout = (SwipeBackLayout) findViewById(R.id.swipeBackLayoutS);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);

        DbAdapter adapter = new DbAdapter(this);

        listView = (ListView) findViewById(R.id.listViewStats);

        startDate = (TextView) findViewById(R.id.showFromDateTVStats);
        endDate = (TextView) findViewById(R.id.showToDateTVStats);
        fromDateTV = (TextView) findViewById(R.id.fromDateTVStats);
        toDateTV = (TextView) findViewById(R.id.toDateTVStats);

        spinnerSortBy = (Spinner) findViewById(R.id.spinnerSortByStats);

        TypefaceHelper.getInstance().setTypeface(spinnerSortBy, "exocet.otf");

        //Will set default date value
        calendar = Calendar.getInstance();
        START_DATE_INTERVAL = sdf.format(calendar.getTime());
        END_DATE_INTERVAL = sdf.format(calendar.getTime());

        startDate.setText(START_DATE_INTERVAL);
        endDate.setText(END_DATE_INTERVAL);

        sortByDate = true;
        //ListView showing todays scan
        scanList = adapter.retrieveScans(START_DATE_INTERVAL + " 00:00",
                END_DATE_INTERVAL + " 24:00", sortByDate);

        listView.setAdapter(new StatsListAdapter(this, scanList));

        initializeListeners();
    }

    public void initializeListeners() {
        //Sends a fragment to DatePickerDialogFragment on click
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DatePickerDialogFragment(StatisticsActivity.this);
                newFragment.show(ft, FRAGMENT_FROM);
            }
        });
        fromDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DatePickerDialogFragment(StatisticsActivity.this);
                newFragment.show(ft, FRAGMENT_FROM);
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DatePickerDialogFragment(StatisticsActivity.this);
                newFragment.show(ft, FRAGMENT_TO);
            }
        });
        toDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DatePickerDialogFragment(StatisticsActivity.this);
                newFragment.show(ft, FRAGMENT_TO);
            }
        });
        spinnerSortBy.setOnItemSelectedListener(new SpinnerOnItemSelect());
    }

    /**
     * Runs after a date is set with date format to "yyyy-MM-dd". <br>
     * Sets {@code TextView} to display date chosen.
     *
     * @param view
     * @param year
     * @param monthOfYear
     * @param dayOfMonth
     */
    //Runs after choosing a date with spinner (FROM and TO) and sets the date
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {
        calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);

        Fragment from = getFragmentManager().findFragmentByTag(FRAGMENT_FROM);
        Fragment to = getFragmentManager().findFragmentByTag(FRAGMENT_TO);

        if (from != null) {
            START_DATE_INTERVAL = sdf.format(calendar.getTime());
            startDate.setText(START_DATE_INTERVAL);

        }
        if (to != null) {
            END_DATE_INTERVAL = sdf.format(calendar.getTime());
            endDate.setText(END_DATE_INTERVAL);
        }
    }

    boolean displayAchiev = false,
            displayEaster = false;

    /**
     * Shows a Toast if an achievement and/or easter egg is found.
     * @param view
     */
    public void onListViewClick(View view) {
        if (toaster != null) {
            toaster.cancel();
        }

        DbAdapter adapter = new DbAdapter(this);
        Scan currentScan = scanList.get(Integer.parseInt(view.getTag().toString()));

        if (currentScan.getAchievementId() != 0) {
            achievement = adapter.retrieveAchievementById(currentScan.getAchievementId());
            achievDescription = achievement.getDescription();
            achievName = achievement.getName();
            displayAchiev = true;
        } else {
            displayAchiev = false;
        }
        if (currentScan.getEasterEggId() != 0) {
            easterEgg = adapter.retrieveEasterEggById(currentScan.getEasterEggId());
            easterDescription = easterEgg.getDescription();
            easterName = easterEgg.getName();
            displayEaster = true;
        } else {
            displayEaster = false;
        }

        if (displayAchiev == true && displayEaster == false) {
            toaster = new Toaster(StatisticsActivity.this, achievDescription + "\n" + "\"" + achievName + "\"", 2500);
            toaster.showToastTop();
        }
        if (displayEaster == true && displayAchiev == false) {
            toaster = new Toaster(StatisticsActivity.this, easterDescription + "\n" + "\"" + easterName + "\"", 2500);
            toaster.showToastTop();
        }
        if (displayAchiev == true && displayEaster == true) {
            toaster = new Toaster(StatisticsActivity.this, achievDescription + "\n" + "\"" + achievName + "\""
                    + "\n" + "\n" + easterDescription + "\n" + "\"" + easterName + "\"", 2500);
            toaster.showToastTop();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_statistics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets boolean {@code sortByDate} to {@code true} if {@code Date} is picked in spinner, else {@code false}. <p>
     * Runs {@code UpdateOnRefreshButtonClick().}
     * @param view
     */
    public void OnRefreshButtonClick(View view) {

        position = String.valueOf(spinnerSortBy.getSelectedItem());

        switch (position) {
            case "Date":
                sortByDate = true;
                break;
            case "EXP":
                sortByDate = false;
                break;
            default:
                sortByDate = true;
                break;
        }
        new UpdateOnRefreshButtonClick().execute();
    }

    /**
     * Retrieves an {@code ArrayList<Scan>} depending on chosen date intervals.<br>
     * {@code ArrayList<Scan>} is sorted by date when {@code sortByDate} is {@code true}, <br>
     *  and is sorted by EXP when {@code sortByDate} is {@code false}
     */
    private class UpdateOnRefreshButtonClick extends AsyncTask<Void, Void, ArrayList<Scan>> {
        @Override
        protected ArrayList doInBackground(Void... strings) {
            DbAdapter adapter = new DbAdapter(StatisticsActivity.this);

            return adapter.retrieveScans(START_DATE_INTERVAL + " 00:00", END_DATE_INTERVAL + " 24:00",
                    sortByDate);
        }

        @Override
        protected void onPostExecute(ArrayList<Scan> arrayList) {
            scanList = arrayList;
            listView.setAdapter(new StatsListAdapter(StatisticsActivity.this, scanList));

        }
    }
}
