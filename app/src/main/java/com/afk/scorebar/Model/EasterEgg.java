package com.afk.scorebar.model;

/**
 * Model class for easter eggs.
 * <p>Is stored in Database each ORM mapped</p>
 */
public class EasterEgg {

    private final int id;
    private final String name,
            description,
            condition;
    private final int exp;


    public EasterEgg(int id, String name, String description, String condition, int exp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.condition = condition;
        this.exp = exp;

    }
    //GETTERS --->
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCondition() {
        return condition;
    }

    public int getExp() {
        return exp;
    }//<--- GETTERS
}
