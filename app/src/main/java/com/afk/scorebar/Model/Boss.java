package com.afk.scorebar.model;

import java.util.Random;

/**
 *<p>Used in {@code Arena} the main methods are for retrieving simulating dealing and getting damage</p>
 */
public class Boss {

    private String name,
                title;

    private int health,
            damage,
            bossNr;

    public Boss(String name, String title, int health, int damage, int bossNr) {
        this.name = name;
        this.title = title;
        this.health = health;
        this.damage = damage;
        this.bossNr = bossNr;
    }

    /**
     * Returns damage values for the boss.
     * @return int
     */
    public int bossAttack() {
        int thirdPart = damage / 3;
        int lowestDamage = damage - (thirdPart + thirdPart);

        Random random = new Random();

        return random.nextInt(damage - lowestDamage + 1) + lowestDamage;
    }

    /**
     * <p>Decreases {code health} of {@code Boss}</p>
     * @param dmg
     */
    public void receiveDamage(int dmg) {
        health -= dmg;
    }

    // GETTERS -->
    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public int getBossNr() {
        return bossNr;
    }

    public int getHP() {
        return health;
    }

    // <-- GETTERS





}
