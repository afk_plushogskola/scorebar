package com.afk.scorebar.model;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 *  A model class for player. <br>
 *  Sets player level, health points and damage bonus depending on current EXP. Which are received
 *  in the constructor.
 *
 *  <p>Contains all the titles the player can get</p>
 */
public class Player {

    //change maxLevel and/or startHP values for balance changes
    private int maxLevel = 99,
                startHP = 50,
            currentXP,
            checkNextNeeded,
            currentLevel,
            damageBonus,
            healthPoints,
            playerAttackDamage,
            minDamage,
            maxDamage,
            neededXPtoLevelUp,
            previousLevelXP,
            criticalHitDamageBonus;

    //change this value if you want player to level up faster
    //(lower value = player levels up faster)
    private double XPconstant = 0.08;

    private String title;

    private TreeMap<Integer, Integer> expMap,
                                hpMap;

    private Iterator it;

    private Random r;


    private boolean criticalStrike;

    /**
     * @param currentXP used to set level, hp and damage bonus.
     */
    public Player(int currentXP) {
        
        this.currentXP = currentXP;

        setExpMap();
        setHpMap();

        setLevel();

        setTitle();
        setDamage();
        setHealthPoints();
        setNeededXPtoLevelUp();
        setPreviousLevelXP();

        r = new Random();

    }

    /**
     * Creates a new TreeMap<Integer, Integer> with key level and value EXP
     */
    private void setExpMap() {
        expMap = new TreeMap<>();

        for (currentLevel = 0; currentLevel < maxLevel +1; currentLevel++) {
            double xp = currentLevel * currentLevel;
            xp = xp / XPconstant;
            int xpForLevel = (int) Math.round(xp);

            expMap.put(currentLevel, xpForLevel);
        }
    }

    /**
     *  Creates a new TreeMap<Integer, Integer> with key level and value HP
     */
    private void setHpMap() {
        hpMap = new TreeMap<>();

        for (currentLevel = 1; currentLevel < maxLevel +1; currentLevel++) {
            startHP += 10;
            hpMap.put(currentLevel, startHP);
        }
    }

    /**
     * Iterates through {@code expMap} and sets player {@code currentLevel}.
     */
    private void setLevel() {
        it = expMap.entrySet().iterator();

        if (currentXP <= 12){
            currentLevel = 1;
        } else {
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                checkNextNeeded = Integer.parseInt(pair.getValue().toString());

                if (currentXP >= checkNextNeeded) {
                    currentLevel = Integer.parseInt(pair.getKey().toString());
                    currentLevel++;
                    if (!it.hasNext()) {
                        currentLevel--;
                    }
                }
            }
        }
    }

    /**
     * Sets the player bonus damage depending on {@code currentLevel}.
     */
    private void setDamage() {
        damageBonus = currentLevel;
    }

    /**
     * Iterates through {@code hpMap} and sets HP depending on {@code currentLevel}.
     */
    private void setHealthPoints() {
        it = hpMap.entrySet().iterator();


        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            checkNextNeeded = Integer.parseInt(pair.getKey().toString());
            if (currentLevel == checkNextNeeded) {
                healthPoints = Integer.parseInt(pair.getValue().toString());
                break;
            }
        }
    }

    /**
     * Sets the player title depending on {@code currentLevel}.
     */
    private void setTitle() {
        switch(currentLevel){
            case 1:case 2:case 3:case 4:
                title = "Slave Scavenger"; break;

            case 5:case 6:case 7:case 8:case 9:
                title = "Homeless Wanderer"; break;

            case 10:case 11:case 12:case 13:case 14:
                title = "Peasant Sweeper"; break;

            case 15:case 16:case 17:case 18:case 19:
                title = "Commoner Scum"; break;

            case 20:case 21:case 22:case 23:case 24:
                title = "Citizen of Scantown"; break;

            case 25:case 26:case 27:case 28:case 29:
                title = "Scan Squire"; break;

            case 30:case 31:case 32:case 33:case 34:
                title = "Noble Scanner"; break;

            case 35:case 36:case 37:case 38:case 39:
                title = "Knight of the Square QR"; break;

            case 40:case 41:case 42:case 43:case 44:
                title = "Templar Knight of Barborough"; break;

            case 45:case 46:case 47:case 48:case 49:
                title = "Count of Barcrest"; break;

            case 50:case 51:case 52:case 53:case 54:
                title = "Baron of Codechester"; break;

            case 55:case 56:case 57:case 58:case 59:
                title = "Duke of Codebury"; break;

            case 60:case 61:case 62:case 63:case 64:
                title = "Archduke of Codefall"; break;

            case 65:case 66:case 67:case 68:case 69:
                title = "Lord of Codeheim"; break;

            case 70:case 71:case 72:case 73:case 74:
                title = "Prince of Scandale"; break;

            case 75:case 76:case 77:case 78:case 79:
            case 80:case 81:case 82:case 83:case 84:
                title = "High Prince of Scanport"; break;

            case 90:case 91:case 92:case 93:case 94:
            case 95:case 96:case 97:case 98:
                title = "King of Barfort"; break;

            default:
                title = "High King of Barcodes"; break;
        }
    }

    /**
     * Iterates through {@code expMap} and sets the experience needed for next level.
     */
    private void setNeededXPtoLevelUp(){
        it = expMap.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            checkNextNeeded = Integer.parseInt(pair.getKey().toString());
            if (currentLevel == checkNextNeeded) {
                neededXPtoLevelUp = Integer.parseInt(pair.getValue().toString());
                break;
            }
        }
    }

    /**
     * Iterates through {@code expMap} and sets the experience needed for previous level.
     */
    private void setPreviousLevelXP(){
        it = expMap.entrySet().iterator();

        for (Map.Entry<Integer, Integer> pair : expMap.entrySet()) {
            checkNextNeeded = Integer.parseInt(pair.getKey().toString());
            if(currentLevel - 1 == checkNextNeeded){
                previousLevelXP = Integer.parseInt(pair.getValue().toString());
                break;
            }
        }
    }

    /**
     * Sets {@code criticalStrike} to {@code true} if player deals a critical strike, else {@code false}.
     *
     * @param playerDamage
     */
    private void criticalHit(int playerDamage) {
        int willItCrit = r.nextInt(100) + 1;

        if (willItCrit <= 20) {
            criticalStrike = true;
            criticalHitDamageBonus = playerDamage / 2;
        }
        else {
            criticalStrike = false;
        }
    }

    /**
     * Sets min and max damage range depending on {@code scanresult} value. <br>
     * Will add damage bonus if {@code criticalStrike} is true.
     *
     * @param scanresult
     * @return int
     */
    public int attack(int scanresult){

        if (scanresult <= 3) {
            minDamage = 1;
            maxDamage = 6;
        } else {
            minDamage = scanresult - 2;
            maxDamage = scanresult + 3;
        }

        playerAttackDamage = r.nextInt(maxDamage - minDamage + 1) + minDamage;
        playerAttackDamage += damageBonus;

        criticalHit(playerAttackDamage);

        if (criticalStrike == true) {
            playerAttackDamage += criticalHitDamageBonus;
        }

        return playerAttackDamage;
    }

    /**
     * Subtracts player HP.
     * @param dmg
     */
    public void receiveDamage(int dmg){
        healthPoints -= dmg;
    }

    //GETTERS --->
    public int getHP(){
        return healthPoints;
    }
    public int getLevel(){
        return currentLevel;
    }
    public String getTitle(){
        return title;
    }
    public int getNeededXPtoLevelUp(){
        return neededXPtoLevelUp;
    }
    public int getPreviousLevelXP(){
        return previousLevelXP;
    }
    public int getCurrentXP(){
        return currentXP;
    }
    public boolean getCriticalStrike() {
        return criticalStrike;
    }

} //<--- GETTERS


