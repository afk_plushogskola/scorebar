package com.afk.scorebar.model;

/**
 * Model class for scans. represents a scan
 * <p>Is instansiated after every scan </p>
 * <p>Used in {@code DbAdapter} where all the field are mapped to a table</p>
 */
public class Scan {

    private final int exp,
            easterEggId,
            achievementId;

    private final long barcode;

    private final String dateTime;

    public Scan(int exp, long barcode, String dateTime, int easterEggId, int achievementId) {
        this.exp = exp;
        this.barcode = barcode;
        this.dateTime = dateTime;
        this.easterEggId = easterEggId;
        this.achievementId = achievementId;
    }
    //GETTERS --->
    public int getExp() {
        return exp;
    }

    public String getDateTime() {
        return dateTime;
    }

    public long getBarcode() {
        return barcode;
    }

    public int getEasterEggId() {
        return easterEggId;
    }

    public int getAchievementId() {
        return achievementId;
    }//<--- GETTERS
}
