package com.afk.scorebar.model;

import android.content.Context;

import com.afk.scorebar.helper.DbAdapter;

import java.util.ArrayList;

/**
 *<p>Contains all the instances  {@code EasterEgg}'s </p>
 * <p>Has connection with Database through {@code DbAdapter} </p>
 * <p>When using in UI thread it must be handled in background thread</p>
 */
public class EasterEggManager {

    private static final EasterEgg[] easterEggs = {
                    new EasterEgg(1, "Elite!", "You found 1337", "1337", 20),
                    new EasterEgg(2, "Jackpot", "You found 777", "777", 10),
                    new EasterEgg(3, "Evil", "You found 666", "666", 10),
                    new EasterEgg(4, "Half evil", "You found 333", "333", 10),
                    new EasterEgg(5, "1408", "You found 1408", "1408", 20),
                    new EasterEgg(6, "Pi", "You found 314", "314", 10),
                    new EasterEgg(7, "Murder", "You found 187", "187", 10),
                    new EasterEgg(8, "Apocalypse", "You found 2012", "2012", 15),
                    new EasterEgg(9, "Emergency", "You found 911", "911", 10),
                    new EasterEgg(10, "Ring polisen", "You found 112", "112", 10),
                    new EasterEgg(11, "23 is everywhere", "You found 23", "23", 10),
                    new EasterEgg(12, "No snakes here", "You found 121", "121", 10),
                    new EasterEgg(13, "New war...?", "You found 1939", "1939", 15)
            };

    /**
     * <p>Checks if the provided barcode contains {@code EasterEgg}</p>
     * <p>Does so by checking Database first</p>
     * @param context must be provided from Activity
     * @param barcode the barcode to be tested
     * @return
     */
    public EasterEgg checkForEasterEggs(Context context, long barcode) {

        DbAdapter adapter = new DbAdapter(context);
        String subject = String.valueOf(barcode);

        if (!easterEggExistsInDB(subject, adapter)) {
            for (EasterEgg egg : easterEggs) {

                if (subject.contains(egg.getCondition())) {
                    adapter.insertEasterEgg(egg);
                    return egg;
                }
            }
        }
        return null;
    }

    private boolean easterEggExistsInDB(String subject, DbAdapter adapter) {

        ArrayList<EasterEgg> dbEasterEggs = adapter.retrieveEasterEggs();

        for (EasterEgg dbEgg : dbEasterEggs) {
            if (subject.contains(dbEgg.getCondition())) {
                return true;
            }
        }
        return false;
    }
}
