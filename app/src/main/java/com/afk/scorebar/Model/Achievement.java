package com.afk.scorebar.model;

/**
 * Model class for achievements.
 * <p>Is ORM in Database</p>
 */
public class Achievement {

    private final int id;
    private final String name,
            description;
    private final int exp;

    public Achievement(int id ,String name, String description, int exp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.exp = exp;
    }
    //GETTERS --->
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getExp() {
        return exp;
    }//<--- GETTERS

}
