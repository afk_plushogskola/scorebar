package com.afk.scorebar.model;

import android.content.Context;

import com.afk.scorebar.helper.DbAdapter;

/**
 * <p>Contains all the existing {@code Achievement} instances</p>
 * <p>When using this class in UI thread, must be handled in background thread
 * due to connection with Database</p>
 */
public class AchievementManager {

    private static final Achievement[] achievements = {
            new Achievement(1, "Newbie", "Scan five times", 10),
            new Achievement(2, "Miner", "Scan 10 times", 10),
            new Achievement(3, "Prospector", "Scan 15 times", 10),
            new Achievement(4, "Collector", "Scan 50 times", 50),
            new Achievement(5, "Centurion", "Scan 100 times", 100)
    };

    /**
     * Checks if {@code Achievement}'s are present in the Database
     * @param context
     * @return
     */
    public Achievement checkAchievements(Context context) {

        DbAdapter adapter = new DbAdapter(context);

        int achId = adapter.checkAchievements();

        if (achId != -1)
            for (Achievement a : achievements) {
                if (a.getId() == achId) {
                    adapter.insertAchievement(a);
                    return a;
                }
            }
        return null;
    }
    //GETTER
    public Achievement[] getAchievementArray() {
        return achievements;
    }
}
