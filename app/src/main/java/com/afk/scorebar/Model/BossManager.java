package com.afk.scorebar.model;

/**
 * <p>Contains instances of {@code Boss}</p>
 * <p>Also contains 2 methods for retrieving boss with corresponding number #progression</p>
 * <p>And returning the amount of bosses</p>
 */
public class BossManager {

    private final Boss[] bosses = {
            new Boss("The Butcher", "\"Flesh Carver\"", 50, 9, 1),
            new Boss("Alexander", "\"The Scanslayer\"", 200, 24, 2),
            new Boss("Belial", "\"Lord of Lies\"", 400, 36, 3),
            new Boss("Azmodan", "\"Lord of Sin\"", 800, 42, 4),
            new Boss("Diablo", "\"The Prime Evil\"", 1600, 51, 5)
    };

    public Boss progression(int progression) {
        for (Boss b : bosses){
            if (b.getBossNr() == progression) {
                return b;
            }
        }
        return null;
    }
    //GETTER
    public int getLength() {
        return bosses.length;
    }
}
