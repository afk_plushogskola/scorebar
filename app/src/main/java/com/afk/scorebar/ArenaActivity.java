package com.afk.scorebar;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afk.scorebar.helper.DbAdapter;
import com.afk.scorebar.helper.DefeatPopUpDialog;
import com.afk.scorebar.helper.KeyHolder;
import com.afk.scorebar.helper.ScoreCalculator;
import com.afk.scorebar.helper.Toaster;
import com.afk.scorebar.helper.VictoryPopUpDialog;
import com.afk.scorebar.model.Boss;
import com.afk.scorebar.model.BossManager;
import com.afk.scorebar.model.Player;
import com.drivemode.android.typeface.TypefaceHelper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *<p>Handles the players attacks against the boss on the UI thread, saves and retrieves progress
 * via {@code AsyncTask}'s</p>
 * <p>Here logic on the player progression is  also handled in the #onPostExecute method of {code UpdateArenaActivity}</p>
 * <p>On victory or defeat shows DialogFragment</p>
 */
public class ArenaActivity extends Activity {

    private ProgressBar bossHealthProgressBar,
            playerHealthProgressBar;

    private TextView combatLog,
            playerProgressTV,
            playerCurrentHealth,
            playerMaxHealth,
            bossCurrentHealth,
            bossMaxHealth;

    private ImageView bossPictureArena;

    private ScrollView scroller;

    private int playerProgression,
            bossAmount;

    private int bossHPYellow,
            bossHPRed,
            playerHPYellow,
            playerHPRed;

    private String VICTORY_DIALOG = "victory",
            DEFEAT_DIALOG = "defeat";

    private boolean criticalStrike;

    private DbAdapter dbAdapter;
    private Player player;
    private Boss boss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TypefaceHelper.initialize(getApplication());
        setContentView(TypefaceHelper.getInstance().setTypeface(this, R.layout.activity_arena, "exocet.otf"));

        bossHealthProgressBar = (ProgressBar) findViewById(R.id.bossHealthProgressbarArena);
        playerHealthProgressBar = (ProgressBar) findViewById(R.id.playerHealthProgressbarArena);

        bossHealthProgressBar.getIndeterminateDrawable().setColorFilter(0xFF00FF00, PorterDuff.Mode.SRC_IN);
        bossHealthProgressBar.getProgressDrawable().setColorFilter(0xFF00FF00, PorterDuff.Mode.SRC_IN);

        combatLog = (TextView) findViewById(R.id.combatLogTVArena);
        playerProgressTV = (TextView) findViewById(R.id.playerProgressArena);
        playerCurrentHealth = (TextView) findViewById(R.id.playerCurrentHealth);
        playerMaxHealth = (TextView) findViewById(R.id.playerMaxHealth);
        bossCurrentHealth = (TextView) findViewById(R.id.bossCurrentHealth);
        bossMaxHealth = (TextView) findViewById(R.id.bossMaxHealth);

        bossPictureArena = (ImageView) findViewById(R.id.bossPictureArena);
        scroller = (ScrollView) findViewById(R.id.scroller);

        initializePlayer();
        new InitializeBoss().execute();
    }

    /**
     * Initializes player. <br>
     * Will get an int from the intent sent from {@link com.afk.scorebar.MainActivity#arenaMode(View view)}.
     */
    private void initializePlayer() {
        player = new Player(getIntent().getExtras().getInt(KeyHolder.playerCurrentXP));
        playerHealthProgressBar.setMax(player.getHP());

        playerHPYellow = player.getHP() / 2;
        playerHPRed = player.getHP() / 5;

        playerMaxHealth.setText(String.valueOf(player.getHP()));
    }



    /**
     * Sets an Image depending on int parameter.
     *
     * @param progression
     */
    private void setBossPortrait(int progression) {
        if (progression == 1) {
            bossPictureArena.setImageResource(R.drawable.butcher1);
        }
        if (progression == 2) {
            bossPictureArena.setImageResource(R.drawable.alexboss);
        }
        if (progression == 3) {
            bossPictureArena.setImageResource(R.drawable.belial2);
        }
        if (progression == 4) {
            bossPictureArena.setImageResource(R.drawable.azmodan3);
        }
        if (progression == 5) {
            bossPictureArena.setImageResource(R.drawable.diablo4);
        }
    }

    /**
     * Creates a fragment to {@link com.afk.scorebar.helper.DefeatPopUpDialog} class.
     */
    private void showDefeatDialog() {
        DialogFragment newFragment = DefeatPopUpDialog.newInstance(playerProgression);
        newFragment.show(getFragmentManager(), DEFEAT_DIALOG);
    }

    /**
     * Creates a fragment to {@link com.afk.scorebar.helper.VictoryPopUpDialog} class.
     */
    private void showVictoryDialog() {
        DialogFragment newFragment = VictoryPopUpDialog.newInstance();
        newFragment.show(getFragmentManager(), VICTORY_DIALOG);
    }

    /**
     * Recreates ArenaActivity.
     */
    public void doRetry() {
        Intent intent = new Intent(this, ArenaActivity.class);
        intent.putExtra(KeyHolder.playerCurrentXP, player.getCurrentXP());
        startActivity(intent);
        new InitializeBoss().execute();
    }

    /**
     * Navigates user to {@link com.afk.scorebar.MainActivity}
     */
    public void doRetreat() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    /**
     * Will start a scan with the help of ZXing library.
     *
     * @param view
     */
    public void scanAttack(View view) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();
    }

    /**
     * The returned scan data is retrieved. <br>
     * Retrieves a {@code String}, removes all special characters and white spaces. <br>
     * Will parse the {@code String} to an {@code int}. <p>
     * Catches {@code NumberFormatException} and {@code NullPointerException} but does not {@code throw}. <p>
     * Calls {@code UpdateArenaActivity()} in the end.
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve result of scanning - instantiate ZXing object
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        //check we have a valid result
        if (scanningResult != null) {
            try {
                //get content from Intent Result
                String scanContent = scanningResult.getContents();

                //regex will remove special characters
                scanContent = scanContent.replaceAll("[^\\d.]", "");
                scanContent = scanContent.replaceAll("[\\s.]", "");

                List<Long> longList = new ArrayList<>();
                long longScanContent = Long.parseLong(scanContent);

                //extracts a single digit from long and adds it to a list
                while (longScanContent > 0) {
                    longList.add(longScanContent % 10);
                    longScanContent = longScanContent / 10;
                }

                Collections.reverse(longList);
                ScoreCalculator sc = new ScoreCalculator();
                //player attacks
                int pAttack = player.attack(sc.finalScore(longList));
                criticalStrike = player.getCriticalStrike();
                //appends to combatLog
                if (criticalStrike == true) {
                    combatLog.append("You critical hit for " + String.valueOf(pAttack) + " damage" + "\n" + "\n");
                } else {
                    combatLog.append("You dealt " + String.valueOf(pAttack) + " damage" + "\n" + "\n");
                }
                boss.receiveDamage(pAttack);

                int bAttack = boss.bossAttack();
                player.receiveDamage(bAttack);
                combatLog.append(boss.getName() + " dealt " + String.valueOf(bAttack) + " damage" + "\n" + "\n");

                //scrolls to bottom of view.
                scroller.post(new Runnable() {
                    @Override
                    public void run() {
                        scroller.fullScroll(View.FOCUS_DOWN);
                    }
                });

                new UpdateArenaActivity().execute();

            } catch (NumberFormatException e) {

            } catch (NullPointerException e) {

            }
        } else {
            //invalid scan data or canceled
            new Toaster(ArenaActivity.this, "No scan data received!", 1000).showToastTop();
        }
    }

    /**
     * Calls method {@link #doRetreat}.
     *
     * @param view
     */
    public void coward(View view) {
        doRetreat();
    }

    /**
     * Set {@code TextViews} and {@code ProgressBars}. <p>
     * Calls {@code showDefeatDialog} if player HP reaches 0. <br>
     * Calls {@code showVictoryDialog} and {@code UpdatePlayerProgression} when boss HP reaches 0.
     */
    private class UpdateArenaActivity extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            playerProgressTV.setText("(" + String.valueOf(boss.getBossNr() + "/" + bossAmount +
                    ") " + boss.getName() + ", " + boss.getTitle()));

            bossHealthProgressBar.setProgress(boss.getHP());
            playerHealthProgressBar.setProgress(player.getHP());

            playerCurrentHealth.setText(String.valueOf(player.getHP()));
            bossCurrentHealth.setText(String.valueOf(boss.getHP()));

            //Boss progressbar animation
            //Yellow color
            if (boss.getHP() > bossHPRed && boss.getHP() < bossHPYellow) {
                bossHealthProgressBar.getIndeterminateDrawable().setColorFilter(0xffffff00, PorterDuff.Mode.SRC_IN);
                bossHealthProgressBar.getProgressDrawable().setColorFilter(0xffffff00, PorterDuff.Mode.SRC_IN);
            }
            //Red color
            if (boss.getHP() < bossHPRed) {
                bossHealthProgressBar.getIndeterminateDrawable().setColorFilter(0xffff0000, PorterDuff.Mode.SRC_IN);
                bossHealthProgressBar.getProgressDrawable().setColorFilter(0xffff0000, PorterDuff.Mode.SRC_IN);
            }

            //Player progressbar animation
            //Yellow color
            if (player.getHP() > playerHPRed && player.getHP() < playerHPYellow) {
                playerHealthProgressBar.getIndeterminateDrawable().setColorFilter(0xffffff00, PorterDuff.Mode.SRC_IN);
                playerHealthProgressBar.getProgressDrawable().setColorFilter(0xffffff00, PorterDuff.Mode.SRC_IN);
            }
            //Red color
            if (player.getHP() < playerHPRed) {
                playerHealthProgressBar.getIndeterminateDrawable().setColorFilter(0xffff0000, PorterDuff.Mode.SRC_IN);
                playerHealthProgressBar.getProgressDrawable().setColorFilter(0xffff0000, PorterDuff.Mode.SRC_IN);
            }

            if (player.getHP() <= 0) {
                showDefeatDialog();
            }

            if (boss.getHP() <= 0) {
                if (playerProgression >= bossAmount) {
                    new UpdatePlayerProgression().execute(1);
                } else {
                    new UpdatePlayerProgression().execute(boss.getBossNr() + 1);
                    showVictoryDialog();
                }
            }
        }
    }

    /**
     * Updates player progression with given int.
     */
    private class UpdatePlayerProgression extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... ainteger) {
            int progress = ainteger[0].intValue();
            dbAdapter = new DbAdapter(ArenaActivity.this);
            dbAdapter.updatePlayerProgress(progress);
            return null;
        }
    }


    /**
     * Initialize boss. Retrieves an int from {@link com.afk.scorebar.helper.DbAdapter}
     * and sets the player progression.
     */
    private class InitializeBoss extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... avoid) {
            DbAdapter adapter = new DbAdapter(ArenaActivity.this);

            return adapter.retrivePlayerProgress();
        }

        @Override
        protected void onPostExecute(Integer ainteger) {
            BossManager manager = new BossManager();
            bossAmount = manager.getLength();

            playerProgression = ainteger;

            boss = manager.progression(playerProgression);

            bossHPYellow = boss.getHP() / 2;
            bossHPRed = boss.getHP() / 5;

            bossHealthProgressBar.setMax(boss.getHP());
            bossMaxHealth.setText(String.valueOf(boss.getHP()));

            setBossPortrait(playerProgression);

            new UpdateArenaActivity().execute();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        new UpdateArenaActivity().execute();
    }
}
