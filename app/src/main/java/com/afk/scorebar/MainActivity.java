package com.afk.scorebar;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afk.scorebar.helper.DbAdapter;
import com.afk.scorebar.helper.KeyHolder;
import com.afk.scorebar.helper.ScoreCalculator;
import com.afk.scorebar.helper.Toaster;
import com.afk.scorebar.model.Achievement;
import com.afk.scorebar.model.AchievementManager;
import com.afk.scorebar.model.EasterEgg;
import com.afk.scorebar.model.EasterEggManager;
import com.afk.scorebar.model.Player;
import com.afk.scorebar.model.Scan;
import com.drivemode.android.typeface.TypefaceHelper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>This class prompts information to the user on the UI thread</p>
 * <p>Contains 2 {@code AsyncTask}'s that  handle transactions with the DB</p>
 * <ol>
 *     <l>{@code SaveScan}</l>
 *     <l> {@code UpdateGeneralStats}</l>
 * </ol>
 */
public class MainActivity extends ActionBarActivity {

    private TextView dateTimeTV,
            latestXPGainedTV,
            levelAndTitle,
            displayHealthPoints,
            displayDmgBonus,
            currentXPProgress,
            requiredXPProgression,
            XPGained;

    private ProgressBar xpBar;

    private int currentXP;

    private Player player;
    private ScoreCalculator sc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TypefaceHelper.initialize(getApplication());
        setContentView(TypefaceHelper.getInstance().setTypeface(this, R.layout.activity_main, "exocet.otf"));

        dateTimeTV = (TextView) findViewById(R.id.scanDateTimeTVMain);
        XPGained = (TextView) findViewById(R.id.XPGainedTitleMain);
        dateTimeTV = (TextView) findViewById(R.id.scanDateTimeTVMain);
        latestXPGainedTV = (TextView) findViewById(R.id.latestXPGainedTVMain);
        currentXPProgress = (TextView) findViewById(R.id.currentXPProgressTVMain);
        requiredXPProgression = (TextView) findViewById(R.id.requiredXPProgressionTVMain);
        levelAndTitle = (TextView) findViewById(R.id.levelTVMain);
        displayHealthPoints = (TextView) findViewById(R.id.displayHealthPointsMain);
        displayDmgBonus = (TextView) findViewById(R.id.displayDamageBonusTVMain);

        xpBar = (ProgressBar) findViewById(R.id.XPProgressbarMain);

        sc = new ScoreCalculator();

        new UpdateGeneralStats().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts {@code ArenaActivity} with extra an intent int.
     * @param view
     */
    public void arenaMode(View view) {
        Intent intent = new Intent(this, ArenaActivity.class);
        intent.putExtra(KeyHolder.playerCurrentXP, currentXP);
        startActivity(intent);
    }

    /**
     * Starts {@code AchievementActivity}.
     * @param view
     */
    public void achievClick(View view) {
        startActivity(new Intent(this, AchievementActivity.class));
    }

    /**
     * Starts {@code StatisticsActivity}
     * @param view
     */
    public void statsClick(View view) {
        startActivity(new Intent(this, StatisticsActivity.class));
    }

    //runs when "scanButtonMain" is clicked
    public void scanClick(View view) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();
    }

    /**
     * The returned scan data is retrieved. <br>
     * Retrieves a {@code String}, removes all special characters and white spaces. <br>
     * Will parse the {@code String} to an {@code int}. <p>
     * Runs {@code SaveScan}
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve result of scanning - instantiate ZXing object
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        //check we have a valid result
        if (scanningResult != null) {
            try {
                //get content from Intent Result
                String scanContent = scanningResult.getContents();

                //regex will remove special characters
                scanContent = scanContent.replaceAll("[^\\d.]", "");
                scanContent = scanContent.replaceAll("[\\s.]", "");

                List<Long> longList = new ArrayList<>();
                long longScanContent = Long.parseLong(scanContent);
                long barcodeValue = longScanContent;

                //extracts a single digit from long and adds it to a list
                while (longScanContent > 0) {
                    longList.add(longScanContent % 10);
                    longScanContent = longScanContent / 10;
                }

                Collections.reverse(longList);
                new SaveScan().execute((long) sc.finalScore(longList), barcodeValue);

            } catch (NumberFormatException e) {

            } catch (NullPointerException e) {

            }

            new UpdateGeneralStats().execute();

        } else {
            //invalid scan data or canceled
            new Toaster(MainActivity.this, "No scan data received!", 1000).showToastTop();
        }
    }

    /**
     * Checks if the returned scan data has an easter egg or achievement.<br>
     * Shows a toast depending if easter egg or achievement was found, and also after each successful scan.
     */
    private class SaveScan extends AsyncTask<Long, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Long... params) {

            DbAdapter adapter = new DbAdapter(MainActivity.this);

            int score = params[0].intValue();
            long barcode = params[1].longValue();

            if (adapter.scanAlreadyInDB(barcode)) {
                return false;
            }

            EasterEggManager eggManager = new EasterEggManager();
            EasterEgg egg = eggManager.checkForEasterEggs(MainActivity.this, barcode);
            int eggId = -1;
            int eggExp = 0;
            if (egg != null) {
                eggId = egg.getId();
                eggExp = egg.getExp();
                publishProgress(1);
            }

            AchievementManager achievementManager = new AchievementManager();
            Achievement achievement = achievementManager.checkAchievements(MainActivity.this);
            int achId = -1;
            int achExp = 0;
            if (achievement != null) {
                achId = achievement.getId();
                achExp = achievement.getExp();
                publishProgress(2);
            }

            Date date = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String now = sdf.format(date);

            Scan s = new Scan(score + eggExp + achExp, barcode, now, eggId, achId);

            long id = adapter.insertScan(s);

            return id > 0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int selectedToast = values[0].intValue();

            if (selectedToast == 1) {
                new Toaster(MainActivity.this, "Easter egg found", 1000).showToastTop();
            } else if (selectedToast == 2) {
                new Toaster(MainActivity.this, "Achievement \n unlocked", 1000).showToastTop();
            }
        }

        @Override
        protected void onPostExecute(Boolean successfullInsert) {
            if (successfullInsert) {
                new Toaster(MainActivity.this, "Successfully \n scanned", 1000).showToastTop();
            } else {
                new Toaster(MainActivity.this, "Barcode \n already scanned", 1000).showToastTop();
            }
        }
    }

    /**
     * Retrieves a {@code HashMap} from {@link DbAdapter}. <p>
     * Sets {@code TextView} and {@code ProgressBar}.
     */
    private class UpdateGeneralStats extends AsyncTask<Void, Void, HashMap> {
        @Override
        protected HashMap doInBackground(Void... params) {

            DbAdapter adapter = new DbAdapter(MainActivity.this);

            return adapter.retriveGeneralStats();
        }

        @Override
        protected void onPostExecute(HashMap hashMap) {
            String latestDate = String.valueOf(hashMap.get(KeyHolder.dateTime));
            String latestXP = String.valueOf(hashMap.get(KeyHolder.score));

            if (latestXP != "null"){
                dateTimeTV.setText(latestDate);
                latestXPGainedTV.setText(latestXP);
                dateTimeTV.setVisibility(View.VISIBLE);
                latestXPGainedTV.setVisibility(View.VISIBLE);
                XPGained.setVisibility(View.VISIBLE);
            } else {
                dateTimeTV.setVisibility(View.INVISIBLE);
                latestXPGainedTV.setVisibility(View.INVISIBLE);
                XPGained.setVisibility(View.INVISIBLE);
            }

            currentXP = Integer.parseInt(hashMap.get(KeyHolder.totalScore).toString());
            player = new Player(currentXP);

            levelAndTitle.setText("Level " + String.valueOf(player.getLevel()) + "\n" + player.getTitle());
            displayHealthPoints.setText(String.valueOf(player.getHP()));
            displayDmgBonus.setText("+" + String.valueOf(player.getLevel()));

            //sets the progression bar for XP
            xpBar.setMax(player.getNeededXPtoLevelUp());

            int currentProgression = Integer.parseInt(hashMap.get(KeyHolder.totalScore).toString()) - player.getPreviousLevelXP();

            xpBar.setProgress(currentProgression);
            xpBar.getIndeterminateDrawable().setColorFilter(0xff990066, PorterDuff.Mode.SRC_IN);
            xpBar.getProgressDrawable().setColorFilter(0xff990066, PorterDuff.Mode.SRC_IN);

            //sets current XP and required XP to level up
            currentXPProgress.setText(String.valueOf(currentXP));
            requiredXPProgression.setText(String.valueOf(player.getNeededXPtoLevelUp()));
        }
    }
}
