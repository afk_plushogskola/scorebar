package com.afk.scorebar.helper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

import com.afk.scorebar.ArenaActivity;
import com.afk.scorebar.R;

/**
 * Returns alert dialog when called. <p>
 * Calls {@code doRetreat()} on negative click and {@code doRetry()} on positive click.
 */
public class VictoryPopUpDialog extends DialogFragment {


    public static VictoryPopUpDialog newInstance() {
        VictoryPopUpDialog frag = new VictoryPopUpDialog();
        return frag;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        return new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom))
                .setTitle("Victory!")
                .setMessage("Glorious victory!")
                .setNegativeButton("Back",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((ArenaActivity) getActivity()).doRetreat();
                            }
                        }
                )
                .setPositiveButton("Continue",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((ArenaActivity) getActivity()).doRetry();
                            }
                        }
                )
                .create();
    }
}
