package com.afk.scorebar.helper;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afk.scorebar.R;
import com.afk.scorebar.model.Achievement;
import com.drivemode.android.typeface.TypefaceHelper;

import java.util.ArrayList;

/**
 * Inflates the {@code ListView} for found achievements with the sent {@code ArrayList<Achievement>} in parameter. <br>
 * Sets {@code TextViews} with name and description and a {@code ImageView} for the icon.
 */
public class AchievementListAdapter extends BaseAdapter {



    private final LayoutInflater layoutInflater;
    ArrayList<Achievement> achievements = new ArrayList<>();

    public AchievementListAdapter(Context c, ArrayList<Achievement> statsList){

        achievements = statsList;
        layoutInflater = LayoutInflater.from(c);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RelativeLayout statsLayout;


        if( convertView == null) {
            statsLayout = (RelativeLayout)layoutInflater.inflate
                    (R.layout.achievlistitem, parent, false);
        } else {
            statsLayout = (RelativeLayout)convertView;
        }

        TextView nameView = (TextView)statsLayout.findViewById(R.id.text1);
        TextView desView = (TextView)statsLayout.findViewById(R.id.text2);
        TextView pointView = (TextView)statsLayout.findViewById(R.id.achievpoint);
        ImageView icon = (ImageView)statsLayout.findViewById(R.id.icon);
        icon.setBackgroundResource(R.drawable.ic_trophy);

        Achievement current = achievements.get(position);
        nameView.setText(current.getName());
        desView.setText(current.getDescription());
        pointView.setText("+ " +Integer.toString(current.getExp())+" EXP");

        statsLayout.setTag(position);
        TypefaceHelper.getInstance().setTypeface(statsLayout, "exocet.otf");

        return statsLayout;
    }

    @Override
    public int getCount() {
        return achievements.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}


