package com.afk.scorebar.helper;

/**
 * <p>Used for common keys across classes</p>
 */
public class KeyHolder {

    public static final String dateTime = "dateTime";
    public static final String score = "score";
    public static final String totalScore = "totalScore";
    public static final String playerCurrentXP = "current xp";

}
