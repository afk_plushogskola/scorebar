package com.afk.scorebar.helper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

import com.afk.scorebar.ArenaActivity;
import com.afk.scorebar.R;

/**
 * Sets dialog message dependant on {@code int} sent in constructor parameter. <p>
 *  Calls {@code doRetreat()} on negative click and {@code doRetry()} on positive click.
 */
public class DefeatPopUpDialog extends DialogFragment {

    private String defeatMessage;

    public static DefeatPopUpDialog newInstance(int progression) {
        DefeatPopUpDialog frag = new DefeatPopUpDialog();
        Bundle args = new Bundle();
        args.putInt("progression", progression);
        frag.setArguments(args);

        return frag;
    }

    /**
     * <p>Depending on {@code progression} will show different {@code defeatMessage}</p>
     * @param savedInstanceState is stord in {@code savedInstanceState}
     * @return {@code DialogFragment}
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int progression = getArguments().getInt("progression");

        if (progression == 1) {
            defeatMessage = "Ahh, fresh meat";
        }
        if (progression == 2) {
            defeatMessage = "Pitiful mortal - Alexander";
        }
        if (progression == 3) {
            defeatMessage = "No one escapes the wages of Sin";
        }
        if (progression == 4) {
            defeatMessage = "All that you have known - all that you have ever loved - shall die along with you";
        }
        if (progression == 5) {
            defeatMessage = "I will not be undone! The world is mine!";
        }

        return new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom))
                .setTitle("Defeat!")
                .setMessage("\"" + defeatMessage + "\"")
                .setNegativeButton("Retreat",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((ArenaActivity) getActivity()).doRetreat();
                            }
                        }
                )
                .setPositiveButton("Retry",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((ArenaActivity) getActivity()).doRetry();
                            }
                        }
                )
                .create();
    }
}

