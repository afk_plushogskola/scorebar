package com.afk.scorebar.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Calculates a score based on the {@code List} received as a parameter and returns it as int.
 */
public class ScoreCalculator {

    public ScoreCalculator() {
    }

    //calculate base score
    public Long setScore(List list) {
        List<Long> scorelist;
        scorelist = list;
        long score;
        int score1 = 0;
        int score2 = 0;
        int weightOdd = 2;
        int weightEven = 3;

        for (int odd = 2; odd < scorelist.size(); odd = odd + 2) {
            long n = weightOdd * scorelist.get(odd);
            score1 += n;
        }
        for (int even = 1; even < scorelist.size(); even = even + 2) {
            long m = weightEven * scorelist.get(even);
            score2 += m;
        }
        score = score1 + score2;
        score = score % 10;
        return score;
    }

    public int finalScore(List list) {
    //Read the barcode, return arraylist of [element; number i row; ...]
        List<Long> numberList = new ArrayList();
        int bonus1 = 0;
        int bonus2 = 0;
        int bonus3 = 0;
        long antal = 1;

        for (int i = 1; i < (list.size()); i++) {
            if ((Long.valueOf((Long) list.get(i - 1)) == ((Long.valueOf((Long) list.get(i)))))) {
                antal = antal + 1;
            } else {
                long j = Long.valueOf((Long) list.get(i - 1));
                numberList.add(j);
                numberList.add(antal);
                antal = 1;
            }
        }

        long j = Long.valueOf((Long) list.get(list.size() - 1));
        numberList.add(j);
        numberList.add(antal);

        //calculate bonus score

        for (int i = 1; i < numberList.size(); i = i + 2) {
            if (Long.valueOf(numberList.get(i)) == 2) {
                bonus1 = bonus1 + 2;
            } else if (Long.valueOf(numberList.get(i)) == 3) {
                bonus2 = bonus2 + 5;
            } else if (Long.valueOf(numberList.get(i)) > 3) {
                bonus3 = bonus3 + 10;
            }
        }
        //final bonus
        long bonus = +bonus1 + bonus2 + bonus3;

        //final score
        long finalScore = bonus + setScore(list);
        return (int)finalScore;
    }
}