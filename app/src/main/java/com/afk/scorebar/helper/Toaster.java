package com.afk.scorebar.helper;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afk.scorebar.R;
import com.drivemode.android.typeface.TypefaceHelper;

/**
 * A customized {@code Toast}. <p>
 * Constructor parameters with {@code Context}, {@code String} for message and {@code int} for duration in Milliseconds.
 */
public class Toaster extends Toast {

    public Toaster(Context activityContext, String message, int duration) {
        super(activityContext);

        this.setDuration(duration);

        LayoutInflater inflater = (LayoutInflater) activityContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.toast, (ViewGroup) ((Activity) activityContext).findViewById(R.id.toast));
        this.setView(view);

        TextView tv = (TextView) view.findViewById(R.id.toastMessage);
        tv.setText(message);
        TypefaceHelper.getInstance().setTypeface(tv, "exocet.otf");
    }

    public void showToastTop() {
        this.setGravity(Gravity.TOP, 0, 0);
        this.show();
    }
}
