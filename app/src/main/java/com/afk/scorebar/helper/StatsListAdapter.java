package com.afk.scorebar.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afk.scorebar.R;
import com.afk.scorebar.model.Scan;
import com.drivemode.android.typeface.TypefaceHelper;

import java.util.ArrayList;

/**
 * Inflates the {@code ListView} for scans with the sent {@code ArrayList<Scan>} constructor in parameter. <br>
 * Sets {@code TextViews} with date and EXP gained and a {@code ImageView} for the icon if an achievement
 * or easter egg was found.
 */
public class StatsListAdapter extends BaseAdapter {

    private final ArrayList<Scan> scans;
    private final LayoutInflater layoutInflater;

    public StatsListAdapter(Context c, ArrayList<Scan> statsList){
        scans = statsList;
        layoutInflater = LayoutInflater.from(c);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout statsLayout;

        if( convertView == null) {
            statsLayout = (LinearLayout)layoutInflater.inflate
                    (R.layout.statslistitem, parent, false);
        } else {
            statsLayout = (LinearLayout)convertView;
        }

        TextView datesView = (TextView)statsLayout.findViewById(R.id.stats_date);
        TextView expView = (TextView)statsLayout.findViewById(R.id.stats_exp);

        ImageView eggView = (ImageView)statsLayout.findViewById(R.id.eggIcon);
        ImageView achievView = (ImageView)statsLayout.findViewById(R.id.achievIcon);


        Scan currentScan = scans.get(position);

        datesView.setText(currentScan.getDateTime());
        expView.setText("EXP: " + Integer.toString(currentScan.getExp()));

        if (currentScan.getEasterEggId() != 0){
            eggView.setImageResource(R.drawable.ic_egg);
        } else {
            eggView.setImageResource(0);
        }

        if (currentScan.getAchievementId() != 0){
            achievView.setImageResource(R.drawable.ic_trophy);
        } else {
            achievView.setImageResource(0);
        }

        statsLayout.setTag(position);
        TypefaceHelper.getInstance().setTypeface(statsLayout, "exocet.otf");
        return statsLayout;
    }

    @Override
    public int getCount() {
        return scans.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
