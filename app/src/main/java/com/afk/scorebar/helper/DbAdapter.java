package com.afk.scorebar.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.afk.scorebar.model.Achievement;
import com.afk.scorebar.model.EasterEgg;
import com.afk.scorebar.model.Scan;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * <p>Handles SQLite queries through public methods, contains inner class {@code DbHelper} </p>
 * <p>{@code DbAdapter} functions as outer layer for the DB</p>
 * <p>All DB transactions must go through this class</p>
 * <p>All public methods close {@code Cursor} and {@code SQLiteDatabase} if used</p>
 */
public class DbAdapter {

    private DbHelper helper;
//    private Context context;

    /**
     * <p>Creates instance of {@code DbHelper}</p>
     *
     * @param context
     */
    public DbAdapter(Context context) {

//        this.context = context;
        helper = new DbHelper(context);

    }

    //EASTER EGG METHODS -->

    /**
     * <p>Inserts {@code EasterEgg} in SQLite Db </p>
     *
     * @param egg must not be null will throw NullPointerException
     * @return long if insert successful or not (-1 is bad)
     */
    public long insertEasterEgg(EasterEgg egg) {

        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbHelper.EASTER_EGG_ID, egg.getId());
        values.put(DbHelper.EASTER_EGG_NAME, egg.getName());
        values.put(DbHelper.EASTER_EGG_DESCRIPTION, egg.getDescription());
        values.put(DbHelper.EASTER_EGG_CONDITION, egg.getCondition());
        values.put(DbHelper.EASTER_EGG_EXP, egg.getExp());


        long id = db.insert(DbHelper.EASTER_EGG_TABLE_NAME, null, values);

        db.close();

        return id;
    }

    /**
     * <p>Retrieves all the rows in the Easteregg table</p>
     *
     * @return ArrayList<{@code EasterEgg} can be empty depending on table
     */
    public ArrayList<EasterEgg> retrieveEasterEggs() {

        ArrayList<EasterEgg> eggList = new ArrayList<>();

        SQLiteDatabase db = helper.getWritableDatabase();


        Cursor cursor = db.query(DbHelper.EASTER_EGG_TABLE_NAME, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            eggList.add(new EasterEgg(
                    cursor.getInt(cursor.getColumnIndex(DbHelper.EASTER_EGG_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_NAME)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_CONDITION)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.EASTER_EGG_EXP))
            ));
        }

        cursor.close();
        db.close();
        return eggList;
    }

    /**
     * <p>Retrieves spesific {@code EasterEgg} by id in DB</p>
     *
     * @param id used to get specific row in {@code EasterEgg} table
     *           is primary key
     * @return {@code EasterEgg} can be null
     */
    public EasterEgg retrieveEasterEggById(int id) {

        EasterEgg egg = null;

        SQLiteDatabase db = helper.getWritableDatabase();

        String where = DbHelper.EASTER_EGG_ID + " = ?";
        String[] args = {String.valueOf(id)};


        Cursor cursor = db.query(DbHelper.EASTER_EGG_TABLE_NAME, null, where, args, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            egg = new EasterEgg(
                    cursor.getInt(cursor.getColumnIndex(DbHelper.EASTER_EGG_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_NAME)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.EASTER_EGG_CONDITION)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.EASTER_EGG_EXP))
            );
        }

        cursor.close();
        db.close();
        return egg;
    }
    //<-- EASTER EGG METHODS


    //ACHIEVEMENT METHODS -->

    /**
     * <p>Inserts {@code Achievement} in DB</p>
     *
     * @param achievement if null throws NullPointerException
     * @return long if insert successful or not (-1 is bad)
     */
    public long insertAchievement(Achievement achievement) {

        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbHelper.ACHIEVEMENT_ID, achievement.getId());
        values.put(DbHelper.ACHIEVEMENT_NAME, achievement.getName());
        values.put(DbHelper.ACHIEVEMENT_DESCRIPTION, achievement.getDescription());
        values.put(DbHelper.ACHIEVEMENT_EXP, achievement.getExp());


        long id = db.insert(DbHelper.ACHIEVEMENT_TABLE_NAME, null, values);

        db.close();

        return id;
    }

    /**
     * <p>Counts rows in scan table, depending on count return value is different</p>
     *
     * @return intIf no requirements are met return value will be -1
     */
    public int checkAchievements() {


        String countScansQuery = "SELECT COUNT(*) FROM " + DbHelper.SCAN_TABLE_NAME;

        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor c = db.rawQuery(countScansQuery, null);

        if (c != null && c.moveToFirst()) {

            if (c.getLong(0) == 5 - 1) {
                return 1;
            } else if (c.getLong(0) == 10 - 1) {
                return 2;
            } else if (c.getLong(0) == 15 - 1) {
                return 3;
            } else if (c.getLong(0) == 50 - 1) {
                return 4;
            } else if (c.getLong(0) == 100 - 1) {
                return 5;
            }
        }

        c.close();
        db.close();
        return -1;
    }

    /**
     * <p>Retrieves all the rows in the Achievement table table</p>
     *
     * @return ArrayList<{@code Achievement} can be empty depending on table
     */
    public ArrayList<Achievement> retrieveAchievements() {

        ArrayList<Achievement> achievementArrayList = new ArrayList<>();

        SQLiteDatabase db = helper.getWritableDatabase();


        Cursor cursor = db.query(DbHelper.ACHIEVEMENT_TABLE_NAME, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            achievementArrayList.add(new Achievement(
                    cursor.getInt(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_NAME)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_DESCRIPTION)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_EXP))
            ));
        }

        cursor.close();
        db.close();
        return achievementArrayList;
    }


    /**
     * <p>Retrieves spesific {@code Achievement} by id in DB</p>
     *
     * @param id used to get specific row in {@code Achievement} table
     *           is primary key
     * @return {@code Achievement} can be null
     */
    public Achievement retrieveAchievementById(int id) {

        Achievement achievement = null;

        SQLiteDatabase db = helper.getWritableDatabase();

        String where = DbHelper.ACHIEVEMENT_ID + " = ?";
        String[] args = {String.valueOf(id)};


        Cursor cursor = db.query(DbHelper.ACHIEVEMENT_TABLE_NAME, null, where, args, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            achievement = new Achievement(
                    cursor.getInt(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_NAME)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_DESCRIPTION)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.ACHIEVEMENT_EXP))
            );
        }

        cursor.close();
        db.close();
        return achievement;
    }
    //<-- ACHIEVEMENT TABLE METHODS

    //PLAYER PROGRESS TABLE METHODS -->

    /**
     * <p>Inserts in player progress table </p>
     *
     * @param bossNr number of {@code Boss}
     * @return long (-1 if error)
     */
    public long insertPlayerProgress(int bossNr) {

        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbHelper.PLAYER_PROGRESSION, bossNr);

        long id = db.insert(DbHelper.PLAYER_TABLE_NAME, null, values);
        db.close();
        return id;

    }

    /**
     * <p>Updates player progress table</p>
     *
     * @param bossNr number of {@code Boss}
     * @return
     */
    public long updatePlayerProgress(int bossNr) {


        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbHelper.PLAYER_PROGRESSION, bossNr);

        String where = DbHelper.PLAYER_PROGRESSION;


        long id = db.update(DbHelper.PLAYER_TABLE_NAME, values, where, null);
        db.close();
        return id;

    }

    /**
     * <p>If player progress does not exist will return 1</p>
     *
     * @return int to indicate the players progress
     */
    public int retrivePlayerProgress() {


        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor cursor = db.query(DbHelper.PLAYER_TABLE_NAME, null, null, null, null, null, null);


        if (cursor != null && cursor.moveToNext()) {
            return cursor.getInt(cursor.getColumnIndex(DbHelper.PLAYER_PROGRESSION));
        }


        insertPlayerProgress(1);
        return 1;


    }
    //<-- PLAYER PROGRESS TABLE METHODS

    //SCAN TABLE METHODS

    /**
     * <p>Checks if barcode already is in DB</p>
     *
     * @param barcode
     * @return false if barcode does not exist in DB else true
     */
    public boolean scanAlreadyInDB(long barcode) {

        SQLiteDatabase db = helper.getWritableDatabase();

        String where = DbHelper.SCAN_TABLE_BARCODE + " = ?";
        String[] args = {String.valueOf(barcode)};

        Cursor cursor = db.query(DbHelper.SCAN_TABLE_NAME, null, where, args, null, null, null);

        if (cursor.getCount() <= 0) {
            db.close();
            cursor.close();
            return false;
        }

        db.close();
        cursor.close();
        return true;

    }

    /**
     * <p>Inserts {@code Scan} in DB</p>
     * @throws NullPointerException if {@code Scan} is null
     * @param s {code Scan}
     * @return long indicating success of transaction (-1 indicates error)
     */
    public long insertScan(Scan s) {

        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbHelper.SCAN_TABLE_EXP, s.getExp());
        values.put(DbHelper.SCAN_TABLE_BARCODE, s.getBarcode());
        values.put(DbHelper.SCAN_TABLE_DATE_TIME, s.getDateTime());

        if (s.getEasterEggId() != -1) {
            values.put(DbHelper.SCAN_TABLE_EASTER_EGG_ID, s.getEasterEggId());
        } else {
            values.putNull(DbHelper.SCAN_TABLE_EASTER_EGG_ID);

        }

        if (s.getAchievementId() != -1) {
            values.put(DbHelper.SCAN_TABLE_ACHIEVEMENT_ID, s.getAchievementId());

        } else {
            values.putNull(DbHelper.SCAN_TABLE_ACHIEVEMENT_ID);

        }


        long id = db.insert(DbHelper.SCAN_TABLE_NAME, null, values);

        db.close();

        return id;
    }

    /**
     * <p>Retrieves ArrayList<{@code Scan}> of all rows in scan table</p>
     * @param timeSpanStart expected format "yyyy-MM-dd 00:00"
     * @param timeSpanEnd expected format "yyyy-MM-dd 24:00"
     * @param sortByDate if true will sort ArrayList<{@code Scan}>by DATE ASC else will
     *                   will sort by EXP DESC
     * @return ArrayList<{@code Scan}> will vary depending on sortByDate, list can be empty
     */
    public ArrayList<Scan> retrieveScans(String timeSpanStart, String timeSpanEnd, boolean sortByDate) {

        ArrayList<Scan> scanList = new ArrayList<>();

        SQLiteDatabase db = helper.getWritableDatabase();

        String where = DbHelper.SCAN_TABLE_DATE_TIME + " BETWEEN ? AND ?";
        String[] args = {timeSpanStart, timeSpanEnd};
        String orderBy;

        if (sortByDate) {
            orderBy = DbHelper.SCAN_TABLE_DATE_TIME + " ASC;";
        } else {
            orderBy = DbHelper.SCAN_TABLE_EXP + " DESC;";
        }

        Cursor cursor = db.query(DbHelper.SCAN_TABLE_NAME, null, where, args, null, null, orderBy);

        while (cursor.moveToNext()) {
            scanList.add(new Scan(
                    cursor.getInt(cursor.getColumnIndex(DbHelper.SCAN_TABLE_EXP)),
                    cursor.getLong(cursor.getColumnIndex(DbHelper.SCAN_TABLE_BARCODE)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.SCAN_TABLE_DATE_TIME)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.SCAN_TABLE_EASTER_EGG_ID)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.SCAN_TABLE_ACHIEVEMENT_ID))
            ));
        }

        cursor.close();
        db.close();
        return scanList;
    }
    //<-- SCAN TABLE METHODS


    /**
     * <p>Retrieves general data from DB</p>
     * <p>Stores data as String as value in HashMap<String, String>, key is used from
     *  {@code KeyHolder.class}</p>
     * @return HashMap<String, String>
     */
    public HashMap retriveGeneralStats() {

        HashMap<String, String> m = new HashMap<>();


        String latestScanQuery = "SELECT * FROM " + DbHelper.SCAN_TABLE_NAME + " WHERE "
                + DbHelper.SCAN_TABLE_DATE_TIME
                + " = ( " + "SELECT MAX(" + DbHelper.SCAN_TABLE_DATE_TIME
                + ") FROM " + DbHelper.SCAN_TABLE_NAME + ");";

        String totalScoreQuery = "SELECT SUM(" + DbHelper.SCAN_TABLE_EXP
                + ") FROM " + DbHelper.SCAN_TABLE_NAME;

        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor c = db.rawQuery(latestScanQuery, null);

        if (c != null && c.moveToLast()) {

            m.put(KeyHolder.dateTime, c.getString(c.getColumnIndex(DbHelper.SCAN_TABLE_DATE_TIME)));
            m.put(KeyHolder.score, String.valueOf(c.getInt(c.getColumnIndex(DbHelper.SCAN_TABLE_EXP))));

        }

        c.close();

        c = db.rawQuery(totalScoreQuery, null);

        if (c != null && c.moveToFirst()) {
            m.put(KeyHolder.totalScore, String.valueOf(c.getLong(0)));
        }

        c.close();
        db.close();


        return m;
    }

    /**
     * <p>Handles creation and updating of the SQLite DB</p>
     * <p>Contains queries for:</p>
     * <ul>
     *     <l>Creating tables
     *     </l>
     *     <l>Dropping tables
     *     </l>
     * </ul>
     * <p>Contains DB version amd DB name</p>
     */
    private static class DbHelper extends SQLiteOpenHelper {

        private Context context;

        private static final String DATABASE_NAME = "ScoreBarDB";
        private static final int DATABASE_VERSION = 10;


        //EASTER EGG TABLE -->
        private static final String EASTER_EGG_TABLE_NAME = "easter_eggs",
                EASTER_EGG_ID = "_id",
                EASTER_EGG_NAME = "name",
                EASTER_EGG_DESCRIPTION = "description",
                EASTER_EGG_CONDITION = "condition",
                EASTER_EGG_EXP = "exp";

        //CREATE QUERY
        private static final String CREATE_EASTER_EGG_TABLE = "CREATE TABLE " + EASTER_EGG_TABLE_NAME + "("
                + EASTER_EGG_ID + " INTEGER PRIMARY KEY, "
                + EASTER_EGG_NAME + " TEXT, "
                + EASTER_EGG_DESCRIPTION + " TEXT, "
                + EASTER_EGG_CONDITION + " TEXT, "
                + EASTER_EGG_EXP + " REAL"
                + ");";

        //DROP QUERY
        private static final String DROP_EASTER_EGG_TABLE = "DROP TABLE IF EXISTS " + EASTER_EGG_TABLE_NAME;
        //<-- EASTER EGG TABLE


        //ACHIEVEMENT TABLE -->
        private static final String ACHIEVEMENT_TABLE_NAME = "achievement",
                ACHIEVEMENT_ID = "_id",
                ACHIEVEMENT_NAME = "name",
                ACHIEVEMENT_DESCRIPTION = "description",
                ACHIEVEMENT_EXP = "exp";

        //CREATE QUERY
        private static final String CREATE_ACHIEVEMENT_TABLE = "CREATE TABLE " + ACHIEVEMENT_TABLE_NAME + "("
                + ACHIEVEMENT_ID + " INTEGER PRIMARY KEY, "
                + ACHIEVEMENT_NAME + " TEXT, "
                + ACHIEVEMENT_DESCRIPTION + " TEXT, "
                + ACHIEVEMENT_EXP + " REAL"
                + ");";

        //DROP QUERY
        private static final String DROP_ACHIEVEMENT_TABLE = "DROP TABLE IF EXISTS " + ACHIEVEMENT_TABLE_NAME;
        //<-- ACHIEVEMENT TABLE

        //PLAYER TABLE -->
        private static final String PLAYER_TABLE_NAME = "player",
                PLAYER_ID = "_id",
                PLAYER_PROGRESSION = "progression";

        //CREATE QUERY
        private static final String CREATE_PLAYER_TABLE = "CREATE TABLE " + PLAYER_TABLE_NAME + "("
                + PLAYER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PLAYER_PROGRESSION + " INTEGER"
                + ");";

        //DROP TABLE
        private static final String DROP_PLAYER_TABLE = "DROP TABLE IF EXISTS " + PLAYER_TABLE_NAME;
        //<-- PLAYER TABLE


        //SCAN TABLE -->
        private static final String SCAN_TABLE_NAME = "scans",
                SCAN_TABLE_ID = "_id",
                SCAN_TABLE_EXP = "exp",
                SCAN_TABLE_BARCODE = "barcode",
        // expected format of String YYYY-MM-DD HH:MM
        SCAN_TABLE_DATE_TIME = "date_time",
                SCAN_TABLE_EASTER_EGG_ID = "easter_egg",
                SCAN_TABLE_ACHIEVEMENT_ID = "achievement";


        //CREATE QUERY
        private static final String CREATE_SCAN_TABLE = "CREATE TABLE " + SCAN_TABLE_NAME + "("
                + SCAN_TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SCAN_TABLE_EXP + " INTEGER, "
                + SCAN_TABLE_BARCODE + " REAL, "
                + SCAN_TABLE_DATE_TIME + " TEXT, "
                + SCAN_TABLE_EASTER_EGG_ID + " REAL, "
                + SCAN_TABLE_ACHIEVEMENT_ID + " REAL, "
                + "FOREIGN KEY(" + SCAN_TABLE_EASTER_EGG_ID + ") "
                + "REFERENCES " + EASTER_EGG_TABLE_NAME + "(" + EASTER_EGG_ID + "), "
                + "FOREIGN KEY(" + SCAN_TABLE_ACHIEVEMENT_ID + ") "
                + "REFERENCES " + ACHIEVEMENT_TABLE_NAME + "(" + ACHIEVEMENT_ID + ")"
                + ");";

        //DROP QUERY
        private static final String DROP_SCAN_TABLE = "DROP TABLE IF EXISTS " + SCAN_TABLE_NAME;
        //<-- SCAN TABLE


        private DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {

                db.execSQL(CREATE_EASTER_EGG_TABLE);
                db.execSQL(CREATE_ACHIEVEMENT_TABLE);
                db.execSQL(CREATE_PLAYER_TABLE);
                db.execSQL(CREATE_SCAN_TABLE);



            } catch (SQLiteException x) {

            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {


                db.execSQL(DROP_EASTER_EGG_TABLE);
                db.execSQL(DROP_ACHIEVEMENT_TABLE);
                db.execSQL(DROP_PLAYER_TABLE);
                db.execSQL(DROP_SCAN_TABLE);

                onCreate(db);


            } catch (SQLiteException x) {

            }
        }
    }
}
