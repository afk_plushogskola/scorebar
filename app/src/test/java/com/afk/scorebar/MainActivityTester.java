package com.afk.scorebar;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by kajri.qu on 2015-05-25.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class MainActivityTester {

    private ShadowActivity shadowMainActivity;

    private Activity mainActivity;

    private Button startAchievTV,
        startStatsTV,
        startArenaTV;

    private String testLevelAndTitleTV = "Level 1\nSlave Scavenger";

    private int testProgressBarMax = 13,
                testProgressBarMin = 0;

    private int isVisible = 0,
                isInvisible = 4;

    @Before
    public void setUp() throws Exception{
        mainActivity = Robolectric.buildActivity(MainActivity.class).create().start().get();
        shadowMainActivity = Shadows.shadowOf(mainActivity);
    }

    @Test
    public void startArenaActivity(){

        startArenaTV = (Button) shadowMainActivity.findViewById(R.id.arenaButtonMain);
        startArenaTV.performClick();

        assertThat(shadowMainActivity.peekNextStartedActivityForResult().intent.getComponent(),
                equalTo(new ComponentName(mainActivity, ArenaActivity.class)));
    }

    @Test
    public void startAchievementActivity() {

        startAchievTV = (Button) shadowMainActivity.findViewById(R.id.achievementButtonMain);
        startAchievTV.performClick();

        assertThat(shadowMainActivity.peekNextStartedActivityForResult().intent.getComponent(),
                equalTo(new ComponentName(mainActivity, AchievementActivity.class)));
    }

    @Test
    public void startStatisticsActivity() {
        startStatsTV = (Button) shadowMainActivity.findViewById(R.id.statsButtonMain);
        startStatsTV.performClick();

        assertThat(shadowMainActivity.peekNextStartedActivityForResult().intent.getComponent(),
                equalTo(new ComponentName(mainActivity, StatisticsActivity.class)));
    }

    @Test
    public void scanButtonMainClickable(){
        Button scanButtonMain = (Button) shadowMainActivity.findViewById(R.id.scanButtonMain);

        assertEquals(true, scanButtonMain.performClick());
    }

    @Test
    public void checkLevelAndTitleTextView() {
        TextView levelAndTitle = (TextView) shadowMainActivity.findViewById(R.id.levelTVMain);
        CharSequence text = levelAndTitle.getText();

        assertEquals(testLevelAndTitleTV, text);
    }

    @Test
    public void checkProgressBarMinAndMax() {
        ProgressBar progressBar = (ProgressBar) shadowMainActivity.findViewById(R.id.XPProgressbarMain);

        int progressBarMax = progressBar.getMax(),
            progressBarMin = progressBar.getProgress();

        assertEquals(testProgressBarMax, progressBarMax);
        assertEquals(testProgressBarMin, progressBarMin);
    }

    @Test
    public void checkIfInvisibleTextView(){
        TextView dateTime = (TextView) shadowMainActivity.findViewById(R.id.scanDateTimeTVMain);
        TextView xpGained = (TextView) shadowMainActivity.findViewById(R.id.XPGainedTitleMain);
        TextView latestXPGained = (TextView) shadowMainActivity.findViewById(R.id.latestXPGainedTVMain);

        assertEquals(isInvisible, dateTime.getVisibility());
        assertEquals(isInvisible, xpGained.getVisibility());
        assertEquals(isInvisible, latestXPGained.getVisibility());

    }
}
