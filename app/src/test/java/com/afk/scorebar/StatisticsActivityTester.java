package com.afk.scorebar;

import android.app.Activity;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertEquals;

/**
 * Created by kajri.qu on 2015-05-26.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class StatisticsActivityTester {

    private ShadowActivity shadowStatsActivity;

    private Activity statsActivity;

    @Before
    public void setUp() throws Exception{
        statsActivity = Robolectric.buildActivity(StatisticsActivity.class).create().start().get();
        shadowStatsActivity = Shadows.shadowOf(statsActivity);

    }

    @Test
    public void fromDateTVIsClickable() {
        TextView fromDateTVStatistics = (TextView) shadowStatsActivity.findViewById(R.id.fromDateTVStats);
        TextView showfromDateTVStatistics = (TextView) shadowStatsActivity.findViewById(R.id.showFromDateTVStats);

        assertEquals(true, fromDateTVStatistics.isClickable());
        assertEquals(true, showfromDateTVStatistics.isClickable());
    }

    @Test
    public void toDateIsTVClickable(){
        TextView toDateTVStatistcs = (TextView) shadowStatsActivity.findViewById(R.id.toDateTVStats);
        TextView showtoDateTVStatistcs = (TextView) shadowStatsActivity.findViewById(R.id.showToDateTVStats);

        assertEquals(true, toDateTVStatistcs.isClickable());
        assertEquals(true, showtoDateTVStatistcs.isClickable());
    }

    @Test
    public void listViewIsClickable(){
        ListView listView = (ListView) shadowStatsActivity.findViewById(R.id.listViewStats);

        assertEquals(true, listView.isClickable());
    }

    @Test
    public void imageButtonRefreshIsClickable(){
        ImageButton refreshButtonStatistics = (ImageButton)
                shadowStatsActivity.findViewById(R.id.refreshButtonStats);

        assertEquals(true, refreshButtonStatistics.isClickable());
    }

}