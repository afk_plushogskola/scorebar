package com.afk.scorebar;

import com.afk.scorebar.helper.DatePickerDialogFragment;
import com.afk.scorebar.helper.DefeatPopUpDialog;
import com.afk.scorebar.helper.VictoryPopUpDialog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.robolectric.util.FragmentTestUtil.startFragment;

/**
 * Created by kajri.qu on 2015-05-25.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class FragmentTester {

    @Test
    public void deafetDialogNotNull(){
        DefeatPopUpDialog fragment = new DefeatPopUpDialog().newInstance(1);
        startFragment(fragment);
        assertNotNull(fragment);
    }

    @Test
    public void victoryDialogNotNull(){
        VictoryPopUpDialog fragment = new VictoryPopUpDialog().newInstance();
        startFragment(fragment);
        assertNotNull(fragment);
    }

    @Test
    public void dateDialogNotNull(){
        DatePickerDialogFragment fragment = new DatePickerDialogFragment();
        startFragment(fragment);
        assertNotNull(fragment);
    }
}
