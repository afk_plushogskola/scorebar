package com.afk.scorebar;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;


import com.afk.scorebar.helper.KeyHolder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;

import org.robolectric.Shadows;
import org.robolectric.annotation.Config;


import org.robolectric.shadows.ShadowActivity;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Created by c0detupus on 2015-05-25.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)

public class ArenaActivityTester {


    private ArenaActivity arenaActivity;
    private ShadowActivity arenaShadow;

    @Before
    public void setUp() throws Exception {


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.putExtra(KeyHolder.playerCurrentXP, 300);

        arenaActivity = Robolectric.buildActivity(ArenaActivity.class).withIntent(intent).create().start().visible().get();

        arenaShadow = Shadows.shadowOf(arenaActivity);

    }

    @Test
    public void retreatButtonClickable() throws Exception {

        Button b = (Button) arenaShadow.findViewById(R.id.retreatButtonArena);

        assertEquals(true, b.isClickable());

    }

    @Test
    public void retreatButtonIntent() throws Exception {

        Button b = (Button) arenaShadow.findViewById(R.id.retreatButtonArena);
        b.performClick();

        assertThat(arenaShadow.peekNextStartedActivityForResult().intent.getComponent(),
                equalTo(new ComponentName(arenaActivity, MainActivity.class)));


    }

    @Test
    public void attackButton() throws Exception {

        Button b = (Button) arenaShadow.findViewById(R.id.attackButtonArena);

        assertEquals(true, b.isClickable());

    }


    @Test
    public void playerProgressArena() throws Exception {

        TextView tv = (TextView) arenaShadow.findViewById(R.id.playerProgressArena);

        String s = "Some string";


        tv.setText(s);


        assertEquals(s, tv.getText().toString());


    }


    @Test
    public void arenaProgressTv() throws Exception {
        TextView tv = (TextView) arenaShadow.findViewById(R.id.arenaProgressTVArena);

        assertEquals("Total arena progress:", tv.getText().toString());

    }


    @Test
    public void bossCurrentHealthTV() throws Exception {
        TextView tv = (TextView) arenaShadow.findViewById(R.id.bossCurrentHealth);

        String h = "100";
        tv.setText(h);

        assertEquals(h, tv.getText().toString());

    }

    @Test
    public void bossCurrentHealthVisible() throws Exception {
        TextView tv = (TextView) arenaShadow.findViewById(R.id.bossCurrentHealth);

        int visible = 0;

        assertEquals(visible, tv.getVisibility());

    }

    @Test
    public void bossMassHealthTV() throws Exception {
        TextView tv = (TextView) arenaShadow.findViewById(R.id.bossMaxHealth);

        String h = "100";
        tv.setText(h);

        assertEquals(h, tv.getText().toString());

    }


    @Test
    public void scrollViewDown() throws Exception {

        ScrollView sv = (ScrollView) arenaShadow.findViewById(R.id.scroller);


        assertTrue(sv.pageScroll(View.FOCUS_DOWN));


    }

    @Test
    public void scrollViewUp() throws Exception {

        ScrollView sv = (ScrollView) arenaShadow.findViewById(R.id.scroller);


        assertFalse(sv.pageScroll(View.FOCUS_UP));


    }

    @Test
    public void bossHealthBar()throws Exception{

        ProgressBar pb = (ProgressBar) arenaShadow.findViewById(R.id.bossHealthProgressbarArena);

        int max = 100;
        int half = max /2 ;

        pb.setMax(100);

        pb.setProgress(half);


        assertEquals(half, pb.getProgress());

        assertEquals(max, pb.getMax());


    }

    @Test
    public void playerHealthBar()throws Exception{

        ProgressBar pb = (ProgressBar) arenaShadow.findViewById(R.id.playerHealthProgressbarArena);

        int max = 100;
        int half = max /2 ;

        pb.setMax(100);

        pb.setProgress(half);

        assertEquals(half, pb.getProgress());

        assertEquals(max, pb.getMax());


    }



}
