package com.afk.scorebar;

import android.widget.Button;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import static org.junit.Assert.assertEquals;

/**
 * Created by felicia.zhu on 2015-05-25.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class AchievementActivityTester {

    private AchievementActivity mActivity;


    @Before
    public void setUp() throws Exception {

        mActivity = Robolectric.buildActivity(AchievementActivity.class)
                .create().get();
  }

    @Test
    public void toggleButtonFirstClick() throws Exception{
        Button button = (Button) mActivity.findViewById(R.id.toggleButton);
        TextView results = (TextView) mActivity.findViewById(R.id.toggleButton);
        TextView label = (TextView)mActivity.findViewById(R.id.achievlabel);

        button.performClick();

        assertEquals("+Achievements+", results.getText().toString());
        assertEquals("Easter Eggs", label.getText().toString());

    }

    @Test
    public void toggleButtonSecondClick() throws Exception{
        Button button = (Button) mActivity.findViewById(R.id.toggleButton);
        TextView results = (TextView) mActivity.findViewById(R.id.toggleButton);
        TextView label = (TextView)mActivity.findViewById(R.id.achievlabel);

        button.performClick();
        button.performClick();

        assertEquals("+Easter Eggs+", results.getText().toString());
        assertEquals("Achievements", label.getText().toString());
    }

}
