# Change Log

## [2.1.2] - 2015-05-28
- Improve performance
- Added CHANGELOG

## [2.0.1] release - 2015-05-21
- Minor bug fix

## [2.0] - 2015-05-21
- Added README to project
- Added breaklines for toast messages
- Minor UI change

## [1.8.6] - 2015-05-21
- Added new alexboss as second boss
- Diablo theme font now apply to toast aswell

## [1.8.4] - 2015-05-21 
- Changed color on refresh button in stats screen

## [1.8.3] - 2015-05-21
- Added SwipeBack for stats, achievement screen

## [1.8.2] - 2015-05-21
- Application now prefers to be installed on the sd card

## [1.8.1] - 2015-05-21
- Added new application icon

## [1.7.8] - 2015-05-21
- Application can now save on sd card

## [1.7.7] - 2015-05-21
- Minor UI change

## [1.7.6] - 2015-05-21
- Now reset player progression if all boss been defeated

## [1.7.5] - 2015-05-21
- Added diablo theme font
- Arena mode got background image
- All toasts got new UI

## [1.7.4] - 2015-05-20
- Added a toast notification when achievement or easter egg icons were clicked

## [1.7.3] - 2015-05-20
- A toast notification will appear whenever achievement, easter egg were found

## [1.7.1] - 2015-05-20
- Now prevent player scan same barcodes duplicate times

## [1.6] - 2015-05-19
- Fixed previous issue that cause player unable to level up when using lollipop

## [1.5.1] - 2015-05-19
- Player can now do critical hit

## [1.5] - 2015-05-19
- Achievements now save in database

## [1.4] - 2015-05-19
- Added popup dialog when player defeated boss
- Player progress now save in database

## [1.3.2] - 2015-05-19
- Changed max level to 99
- Added more titles
- Added color to buttons in mainscreen

## [1.3] - 2015-05-19
- Added achievement screen that display achievement and easter egg
- Added scrolling combat log in arena mode
- Added popup dialog when player got defeated

## [1.2.8] - 2015-05-18
- Added image to boss
- Arena mode now shows player and boss health status

## [1.2.7] - 2015-05-18
- Created tables in database

## [1.2.6] - 2015-05-18
- Added new boss
- Mainscreen now show player level progress

## [1.2.5] - 2015-05-18
- Added easter eggs

## [1.2.4] - 2015-05-14
- Fixed an issue cause latest scan show uncorrectly

## [1.2.3] - 2015-05-13
- Fixed previous issue cause player unable to level up

## [1.2.2] - 2015-05-13
- Added titles to player

## [1.2.1] - 2015-05-12
- Added min/max damage attribute to player

## [1.2] - 2015-05-12
- Added new player model

## [1.1] - 2015-05-12
- Added new boss model
- Added arena mode

## [1.0]  release - 2015-05-07

